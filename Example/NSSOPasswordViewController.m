//
//  NSSOPasswordViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 25/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOPasswordViewController.h"
#import "NSSOCrossAppLoginManager.h"

@interface NSSOPasswordViewController ()
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *currentPassword;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@end

@implementation NSSOPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changePassword:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] changePassword:_oldPassword.text newPassword:_currentPassword.text confirmPassword:_confirmPassword.text success:^{
        [self showAlertMessage:@"Password changed"];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}



- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)showAlertMessage:(NSString *)message{
        [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

@end
