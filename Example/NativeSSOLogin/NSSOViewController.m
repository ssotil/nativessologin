//
//  NSSOViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 10/13/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSString+NSSOValidator.h"
#import "NSSORegisterUserViewController.h"
#import "NSSOSocialLoginManager.h"
#import "NSSOLoginViewController.h"
#import "NSSOPasswordViewController.h"
#import "NSSORecoverPasswordViewController.h"
#import "NSSOSettingViewController.h"
#import "NSSOTempGlobal.h"
@interface NSSOViewController ()
@property (weak, nonatomic) IBOutlet UITextField *emailMobile;

@end

@implementation NSSOViewController
NSSOCrossAppLoginManager *slvc;
UIAlertView *alertView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    slvc = [NSSOCrossAppLoginManager sharedLoginManager];
    
}
- (IBAction)testLoginScenario:(id)sender {
    NSSOLoginViewController *lvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NSSOLoginViewController"];
    [self presentViewController:lvc animated:true completion:^{}];
    
}
- (IBAction)testSignup:(id)sender {
    NSSORegisterUserViewController *ruvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"RegisterUserViewController"];
    
    [self presentViewController:ruvc animated:true completion:^{}];
    
}
- (IBAction)signout:(id)sender {
    
    [slvc signOutUser:^{
        [self showAlertMessage:@"Logout"];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}
- (IBAction)block:(id)sender {
    
    [slvc blockUser:^{
        [self showAlertMessage:@"Blocked"];
    } failure:^(NSError * error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}
- (IBAction)setPassword:(id)sender {
    NSSOPasswordViewController *pvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NSSOPasswordViewController"];
    [self presentViewController:pvc animated:true completion:^{}];
}
- (IBAction)forgotPassword:(id)sender {
    NSSORecoverPasswordViewController *rpvc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NSSORecoverPasswordViewController"];
    [self presentViewController:rpvc animated:true completion:^{}];
}
- (IBAction)getUserDetails:(id)sender
{
    if (deprecatedAllowed)
    {
        [slvc getUserDetails:^(NSDictionary *info){
            [self showAlertMessage:[NSString stringWithFormat:@"User details : %@ ",info]];
        } failure:^(NSError * error) {
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }];
    }
    else
    {
        [slvc getUserDetailsOnCompletion:^(SSOUserDetails * _Nullable user, SSOError * _Nullable error)
        {
            if (error) {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }
            else
            {
            [self showAlertMessage:[self getUserString:user]];
            }
        }];
    }
}
-(NSString *)getUserString:(SSOUserDetails *)user
{
    
//    NSDictionary *emailList = user.emailList;
    
    return  [NSString stringWithFormat:@"firstName:%@\nlastName:%@\nssoid:%@\ndp:%@\ndob:%@\nmobileList:%@\nemailList:%@\nmobileData:%@\nfbConnected:%@\ngpConnected:%@\ncity:%@\npasswordExists:%@\ngender:%@\nprimaryEmail=%@\nprimeProfile=%@\nshareDataAllowed=%@\ntermsAccepted=%@\ntimespointsPolicy=%@\nisEuUser=%@\n",user.firstName,user.lastName,user.ssoid,user.dp,user.dob,user.mobileList,user.emailList,user.mobileData,user.fbConnected,user.gpConnected,user.city, user.passwordExists, user.gender, user.primaryEmail,user.primeProfile,user.shareDataAllowed,user.termsAccepted,user.termsAccepted,user.isEuUser];
}
- (IBAction)getUserDetailsLocal:(id)sender {
    [self showAlertMessage:[self getUserString:[slvc getUserDetailsLocal]]];
}

-(IBAction)openSetting:(id)sender{
    NSSOSettingViewController *svc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NSSOSettingViewController"];
    [self presentViewController:svc animated:true completion:^{}];


}
-(void)showAlertMessage:(NSString *)message{
    if (!alertView){
        alertView = [[UIAlertView alloc] init];
    }
    alertView.title = @"Alert!";
    alertView.message = message;
    alertView.delegate = self;
    [alertView addButtonWithTitle:@"Ok"];
    [alertView show];
}
- (IBAction)myUnwindAction:(UIStoryboardSegue*)unwindSegue
{
//unwindSegue
}
@end
