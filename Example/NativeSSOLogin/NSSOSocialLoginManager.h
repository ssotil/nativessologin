//
//  NSSOSocialLoginManager.h
//  Pods
//
//  Created by Pankaj Verma on 09/11/16.
//
//

#import <UIKit/UIKit.h>
@protocol GoogleLoginDelegate <NSObject>

-(void) googleSignInSuccessfulWithInfo:(NSDictionary *)info;
-(void) googleSignInFailWithError:(NSError *) error;

@end

@protocol TruecallerLoginDelegate <NSObject>

-(void) truecallerSignInSuccessfulWithInfo:(NSDictionary *)info;
-(void) truecallerSignInFailWithError:(NSError *) error;

@end



@interface NSSOSocialLoginManager : NSObject
@property (nonatomic, weak) id <GoogleLoginDelegate> privateGoogleLoginDelegate;
@property (nonatomic, weak) id <TruecallerLoginDelegate> privateTruecallerLoginDelegate;
+ (id) sharedManager;
-(void)loginToFacebookOnViewController:(UIViewController *)vc
                             completion:(void(^)(NSMutableDictionary *info,NSError * error))completion;
-(void)loginToGoogleOnViewController:(id)vc ;
-(void)loginToTruecallerOnViewController:(id)vc;

-(void)doPassiveLoginOnCompletion:(void(^)(NSDictionary *info,NSError * err))completion;
@end
