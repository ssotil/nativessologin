//
//  NSSOUpdateProfileViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 19/12/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOUpdateProfileViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOTempGlobal.h"
@interface NSSOUpdateProfileViewController ()<UITextFieldDelegate>{
    UIDatePicker *datePicker;
    
}
@property (weak, nonatomic) IBOutlet UITextField *birthDateTextfield;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *genderTextfield;
@property (weak, nonatomic) IBOutlet UITextField *cityTextfield;
@property (weak, nonatomic) IBOutlet UITextField *shareDataAllowed;
@property (weak, nonatomic) IBOutlet UITextField *termsAccepted;
@property (weak, nonatomic) IBOutlet UITextField *timespointsPolicy;

@end

@implementation NSSOUpdateProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.birthDateTextfield.delegate = self;
    
    // alloc/init your date picker, and (optional) set its initial date
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]]; //this returns today's date
    
    // theMinimumDate (which signifies the oldest a person can be) and theMaximumDate (defines the youngest a person can be) are the dates you need to define according to your requirements, declare them:
    
    // the date string for the minimum age required (change according to your needs)
    NSString *maxDateString = @"01-Jan-1996";
    // the date formatter used to convert string to date
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // the specific format to use
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    // converting string to date
    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
    
    // repeat the same logic for theMinimumDate if needed
    
    // here you can assign the max and min dates to your datePicker
    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
    [datePicker setMinimumDate:[[NSDate alloc]init]]; //the max age restriction (if needed, or else dont use this line)
    
    
    // set the mode
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    
    // update the textfield with the date everytime it changes with selector defined below
    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
    
    // and finally set the datePicker as the input mode of your textfield
    [self.birthDateTextfield setInputView:datePicker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

-(void)updateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)self.birthDateTextfield.inputView;
    self.birthDateTextfield.text = [self formatDate:picker.date];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}


-(IBAction)update:(id)sender
{
    if (deprecatedAllowed)
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] updateFirstName:self.firstNameTextfield.text
                                                              lastName:_lastNameTextfield
         .text
                                                                   dob:_birthDateTextfield.text
                                                                gender:_genderTextfield.text
                                                               success:^{
                                                                   [self showAlertMessage:@"profile updated successfully"];
                                                               } failure:^(NSError *error) {
                                                                   [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                                                                   
                                                               }];        
    }
    else
    {
    SSOUserUpdates *userD = [[SSOUserUpdates alloc] init];
    userD.firstName = _firstNameTextfield.text;
    userD.lastName = _lastNameTextfield.text;
    userD.gender = _genderTextfield.text;
    userD.city = _cityTextfield.text;
    userD.dob = _birthDateTextfield.text;
        userD.termsAccepted = _termsAccepted.text;
        userD.shareDataAllowed = _shareDataAllowed.text;
        userD.timespointsPolicy = _timespointsPolicy.text;


   [[NSSOCrossAppLoginManager sharedLoginManager] updateUserDetails:userD success:^(SSOUserUpdates * _Nullable user) {
       [self showAlertMessage:[self getUserString:user]];
   } failure:^(SSOError * _Nullable error) {
       [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
   }];
    }
}
-(NSString *)getUserString:(SSOUserUpdates *)user
{
    return  [NSString stringWithFormat:@"firstName:%@\nlastName:%@\ndob:%@\ncity:%@\ngender:%@\nshareDataAllowed:%@\ntermsAccepted:%@\ntimespointsPolicy:%@\n",user.firstName,user.lastName, user.dob, user.city,user.gender,user.shareDataAllowed,user.termsAccepted,user.timespointsPolicy];
}

-(void)showAlertMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}
@end
