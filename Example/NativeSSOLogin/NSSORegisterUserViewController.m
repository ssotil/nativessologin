//
//  NSSORegisterUserViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 20/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSORegisterUserViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOTempGlobal.h"
@interface NSSORegisterUserViewController ()
@property (weak, nonatomic) IBOutlet UITextField *mobile;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UISwitch *gender;
@property (weak, nonatomic) IBOutlet UITextField *signupOtp;
@property (weak, nonatomic) IBOutlet UITextField *shareDataAllowed;
@property (weak, nonatomic) IBOutlet UITextField *termsAccepted;
@property (weak, nonatomic) IBOutlet UITextField *timespointsPolicy;
//@property (weak, nonatomic) IBOutlet UISwitch *isSendOfferEnabled;

@end

@implementation NSSORegisterUserViewController

NSMutableDictionary *info;

- (void)viewDidLoad {
    [super viewDidLoad];
    info = [[NSMutableDictionary alloc] init];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}



- (IBAction)registerUser:(id)sender
{
 [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    NSString *gender ;
    if([_gender isOn])
        gender = @"M";
    else
        gender = @"F";
     if (deprecatedAllowed)
     {
    [[NSSOCrossAppLoginManager sharedLoginManager] registerUser:_name.text mobile:_mobile.text email:_email.text password:_password.text gender:gender isSendOfferEnabled:YES success:^{
        [self showAlertMessage:@"verify otp"];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    }
    else
    {
        
        
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.name = _name.text;
    user.mobile = _mobile.text;
    user.email = _email.text;
    user.password = _password.text;
    user.gender = gender;
        user.termsAccepted = _termsAccepted.text;
        user.shareDataAllowed = _shareDataAllowed.text;
        user.timespointsPolicy = _timespointsPolicy.text;
        
    [[NSSOCrossAppLoginManager sharedLoginManager] performSignupActivity:SSOFullSignup forUser:user success:^{
        [self showAlertMessage:@"verify otp"];
        
    } failure:^(SSOError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        
    }];
    }
}


- (IBAction)registerOnlyMobile:(id)sender
{
//    if (deprecatedAllowed)
//    {
//    [[NSSOCrossAppLoginManager sharedLoginManager] registerOnlyMobile:_mobile.text name:_name.text gender:@"" success:^{
//        [self showAlertMessage:@"verify otp"];
//    } failure:^(NSError * _Nullable error) {
//         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
//    }];
//    }
//    else
//    {
    
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    SSOSignupUser * user = [[SSOSignupUser alloc] init];
    user.name = _name.text;
    user.mobile = _mobile.text;
    user.email = _email.text;
    user.password = _password.text;
    user.termsAccepted = _termsAccepted.text;
    user.shareDataAllowed = _shareDataAllowed.text;
    user.timespointsPolicy = _timespointsPolicy.text;

    [[NSSOCrossAppLoginManager sharedLoginManager] performSignupActivity:SSOOnlyMobileSignup forUser:user success:^{
        [self showAlertMessage:@"verify otp"];
        
    } failure:^(SSOError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        
    }];
  //  }
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

//OTP on Mobile
- (IBAction)verifySignupOtpForMobile:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] verfiySignUpOTP:_signupOtp.text  email: @"" mobile:_mobile.text success:^{
        [self showAlertMessage:@"Signup successfull "];

    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
 
}
- (IBAction)resendSignupOtpOnMobile:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] resendSignUpOtpForEmail:@"" mobile:_mobile.text success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on :%@",_mobile.text]];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}
//OTP on Email
- (IBAction)verifySignupOtpForEmail:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] verfiySignUpOTP:_signupOtp.text  email:_email.text mobile:@"" success:^{
        [self showAlertMessage:@"Signup successful "];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];

}
- (IBAction)resendSignupOtpOnEmail:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] resendSignUpOtpForEmail:_email.text mobile:@"" success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on :%@",_email.text]];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

-(void)showAlertMessage:(NSString *)message{
        [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}



@end
