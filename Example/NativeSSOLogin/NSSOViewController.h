//
//  NSSOViewController.h
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 10/13/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

@import UIKit;

@interface NSSOViewController : UIViewController
- (IBAction)myUnwindAction:(UIStoryboardSegue*)unwindSegue;
@end
