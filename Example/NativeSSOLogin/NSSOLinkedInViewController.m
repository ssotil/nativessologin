//
//  NSSOLinkedInViewController.m
//  NativeSSOLogin_Example
//
//  Created by Pankaj Verma on 18/07/18.
//  Copyright © 2018 Pankaj Verma. All rights reserved.
//

#import "NSSOLinkedInViewController.h"

@interface NSSOLinkedInViewController ()

@end

@implementation NSSOLinkedInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
