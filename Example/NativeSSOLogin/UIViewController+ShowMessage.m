//
//  UIViewController+ShowMessage.h
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 19/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "UIViewController+ShowMessage.h"

@implementation UIViewController (ShowMessage)

-(void)showAlertMessage:(NSString *)message {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Alert!!"
                                                                    message:message
                                                             preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle: @"OK"
                                              style: UIAlertActionStyleDefault
                                            handler: ^(UIAlertAction * _Nonnull action)
                      {
                          [alert dismissViewControllerAnimated:YES completion:nil];
                      }]
     ];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
