//
//  NSSOSocialConnectViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 25/01/17.
//  Copyright © 2017 Pankaj Verma. All rights reserved.
//

#import "NSSOSocialConnectViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOSocialLoginManager.h"
#import "NSSOTempGlobal.h"
@interface NSSOSocialConnectViewController ()<GoogleLoginDelegate>

@end

@implementation NSSOSocialConnectViewController
bool gpUpload = false;
NSMutableDictionary* fbInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)connectFB:(id)sender {
    [[NSSOSocialLoginManager sharedManager] loginToFacebookOnViewController:self completion:^(NSMutableDictionary *info, NSError *error)
     {
         
        if (error == NULL)
        {
            if (deprecatedAllowed) {
                
            fbInfo = info;
            [[NSSOCrossAppLoginManager sharedLoginManager] linkSocialAccountUsingInfo:info success:^{
               [self showAlertMessage:@"FB Linked"];
            } failure:^(NSError *error) {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }];
            }
            else
            {
                            SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                            si.accessToken = [info valueForKey:@"accessToken"];
                            si.oauthId = [info valueForKey:@"oauthId"];
                            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOFacebookLink usingSocialInfo:si success:^{
                                [self showAlertMessage:@"FB Linked"];
                            } failure:^(SSOError * _Nullable error) {
                                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                            }];
            }

        }
        else{
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }
    }];

}
- (IBAction)connectGP:(id)sender {
    gpUpload = false;
    [[NSSOSocialLoginManager sharedManager] loginToGoogleOnViewController:self];
}
- (IBAction)delinkFB:(id)sender {
    if (deprecatedAllowed) {
        [[NSSOCrossAppLoginManager sharedLoginManager] delinkFacebook:^{
            [self showAlertMessage:@"FB deLinked"];
        } failure:^(NSError *error) {
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }];

    }
    else
    {
            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOFacebookDelink usingSocialInfo:nil success:^{
                [self showAlertMessage:@"FB deLinked"];
            } failure:^(SSOError * _Nullable error) {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }];
    }
    

}
- (IBAction)delinkGP:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOGoogleDelink usingSocialInfo:nil success:^{
        [self showAlertMessage:@"Google deLinked"];
    } failure:^(SSOError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

#pragma Sign in via Google delegate methods
-(void) googleSignInSuccessfulWithInfo:(NSMutableDictionary *)info
{
    if(gpUpload)
   {
       if (deprecatedAllowed)
       {
           [[NSSOCrossAppLoginManager sharedLoginManager] uploadProfilePicFromSocialUsingInfo:info success:^{
               [self showAlertMessage:@"Pic uplaoded from Google plus"];
           } failure:^(NSError *error) {
               [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
           }];

       }
       else
       {
                  SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                  si.accessToken = [info valueForKey:@"accessToken"];
                  si.oauthId = [info valueForKey:@"oauthId"];
                  [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOGooglePicUpload usingSocialInfo:si success:^{
                      [self showAlertMessage:@"Pic uplaoded from Google"];
                  } failure:^(SSOError * _Nullable error) {
                      [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                  }];

       }
       
    }
    else
    {
        if (deprecatedAllowed) {
            [[NSSOCrossAppLoginManager sharedLoginManager] linkSocialAccountUsingInfo:info success:^{
                [self showAlertMessage:@"Googleplus Linked"];
            } failure:^(NSError *error) {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }];
        }
        else
        {
                    SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                    si.accessToken = [info valueForKey:@"accessToken"];
                    si.oauthId = [info valueForKey:@"oauthId"];
                    [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOGoogleLink usingSocialInfo:si success:^{
                        [self showAlertMessage:@"Googleplus Linked"];
                    } failure:^(SSOError * _Nullable error) {
                        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                    }];

        }
        
    }
}


-(void) googleSignInFailWithError:(NSError *) error{
    [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
}
-(void)showAlertMessage:(NSString *)message{
        [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}


- (IBAction)uploadFromGoogleplus:(id)sender {
    gpUpload = true;
     [[NSSOSocialLoginManager sharedManager] loginToGoogleOnViewController:self];
    
    
}
- (IBAction)uploadFromFacebook:(id)sender {
    
    [[NSSOSocialLoginManager sharedManager] loginToFacebookOnViewController:self completion:^(NSMutableDictionary *info, NSError *error) {
        if (error == NULL) {
    
            if (deprecatedAllowed) {
                [[NSSOCrossAppLoginManager sharedLoginManager] uploadProfilePicFromSocialUsingInfo:info success:^{
                    [self showAlertMessage:@"Pic uplaoded from Facebook"];
                } failure:^(NSError *error) {
                    [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                }];
            }
            else
            {
                            SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                            si.accessToken = [info valueForKey:@"accessToken"];
                            si.oauthId = [info valueForKey:@"oauthId"];
                            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOFacebookPicUpload usingSocialInfo:si success:^{
                                 [self showAlertMessage:@"Pic uplaoded from Facebook"];
                            } failure:^(SSOError * _Nullable error) {
                                 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                            }];

            }
            
            
        }
        else{
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }
    }];

}
- (IBAction)uploadProfilePic:(id)sender
{
    if (deprecatedAllowed)
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] openPhotoSelectorOnViewController:self success:^{
            [self showAlertMessage:@"Pic uploaded successfully"];
        } failure:^(NSError *error)
         {
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }];
    }
    else
    {
        [[NSSOCrossAppLoginManager sharedLoginManager ] performPickUploadActivity:SSODefault onController:self  startRequest:^{
            NSLog(@"Upload started....");
        } success:^(NSDictionary * _Nullable info) {
                [self showAlertMessage:[NSString stringWithFormat:@"Pic uploaded successfully with url :%@",info[@"picUrl"]]];
                
            } failure:^(SSOError * _Nullable error)
             {
                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
            }];

    }
}

- (IBAction)uploadProfilePicFromCamera:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager ] performPickUploadActivity:SSOCamera onController:self startRequest:^{
        NSLog(@"Upload started....");
    } success:^(NSDictionary * _Nullable info)
    {
        [self showAlertMessage:[NSString stringWithFormat:@"Pic uploaded successfully with url :%@",info[@"picUrl"]]];
        
    } failure:^(SSOError * _Nullable error)
     {
         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
     }];
}
- (IBAction)uploadProfilePicFromGallery:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager ] performPickUploadActivity:SSOPhotoGallery onController:self  startRequest:^{
        NSLog(@"Upload started....");
    } success:^(NSDictionary * _Nullable info) {
        [self showAlertMessage:[NSString stringWithFormat:@"Pic uploaded successfully with url :%@",info[@"picUrl"]]];
        
    } failure:^(SSOError * _Nullable error)
     {
         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
     }];
    
}

@end
