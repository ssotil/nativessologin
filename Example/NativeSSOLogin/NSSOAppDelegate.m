//
//  NSSOAppDelegate.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 10/13/2016.
//  Copyright (c) 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOAppDelegate.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOSocialLoginManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import <TrueSDK/TrueSDK.h>
#import "NSSOTempGlobal.h"
#import <linkedin-sdk/LISDK.h>

@implementation NSSOAppDelegate
NSSOCrossAppLoginManager * loginManager;
NSUInteger crossapp = 0;
NSString *oldTicket = @"";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //facebook
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //Google
    NSString *googleClientID = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"GoogleClientID"];
    [GIDSignIn sharedInstance].clientID = googleClientID;
    
    // Truecaller
    if ([[TCTrueSDK sharedManager] isSupported])
    {
        NSString *appKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"TrueAppKey"];
        NSString *appLink = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"TrueAppLink"];
        [[TCTrueSDK sharedManager] setupWithAppKey:appKey appLink:appLink];
    }
    
    //    CFStringRef v = (CFStringRef)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), kCFBundleVersionKey);
    //    NSString * ver = [NSString stringWithUTF8String:CFStringGetCStringPtr(v, kCFStringEncodingMacRoman)];
    //    NSLog(@"version = %@",ver);
    
    //    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    //    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    //    NSLog(@"version = %@",version);
    
    
    
    //1
    loginManager = [NSSOCrossAppLoginManager sharedLoginManager];
    
    //    //2
    //        [loginManager ssoSetupForChannel:@"toicrossapp"
    //                                  siteId:@"bb51be5a5a8a0283467e0859d262ff6g"
    //                                  teamId:@"RB3CUQ8JTM"
    //                              completion:^(NSDictionary * _Nonnull info, NSError * _Nonnull error)
    //         {
    //
    //                              }
    //
    
    
    //    if (deprecatedAllowed)
    //    {
    //        [loginManager ssoSetupForChannel:@"gaana"
    //                                  siteId:@"5995f55f00c9d5ce945c778ed96a76f2"
    //                                  teamId:@"RB3CUQ8JTM"//@"H686S25ZPL"
    //                              completion:^(NSDictionary *info, NSError * error)
    //
    //
    
    
    
    loginManager = [NSSOCrossAppLoginManager sharedLoginManager];
    [loginManager ssoSetupForChannel:@"toi"
                                siteId:@"541cc79a8638cfd34bdc56d1a27c8cd7"
                                teamId:@"2C5Z9X929T"
                              isLive:false
                            completion:^(NSDictionary * _Nonnull info, NSError * _Nonnull error)
     
//         [loginManager ssoSetupForChannel:@"et-mobile"
//                                   siteId:@"6033730636a8a4fe139e5289fcc4ae55"
//                                   teamId:@"2C5Z9X929T"
//                               completion:^(NSDictionary * _Nonnull info, NSError * _Nonnull error)
     {
         if (error == NULL)
         {
             //3 old ticket found
             
             [self continueLogin];
             
         }
     }];
    //    }
    //    else
    //    {
    //    [loginManager initializeSDKOnSuccess:^{
    //                                  }
    //                                  failure:^(SSOError * _Nullable error)
    //     {
    //         [self showAlertMessage:error.localizedDescription];
    //     }];
    //
    //    }
    
    
    return true;
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    BOOL canHandleFBURL = [[FBSDKApplicationDelegate sharedInstance]application:application
                                                                        openURL:url
                                                              sourceApplication:sourceApplication
                                                                     annotation:annotation];
    BOOL canHandleGIDURL = [[GIDSignIn sharedInstance] handleURL:url
                                               sourceApplication:sourceApplication
                                                      annotation:annotation];
    BOOL canHandleLinkedInURL = [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation];

    BOOL canHandleURL = canHandleFBURL || canHandleGIDURL || canHandleLinkedInURL;
    return canHandleURL;
}
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
    return [[TCTrueSDK sharedManager] application:application continueUserActivity:userActivity restorationHandler:restorationHandler];
}
-(void)showAlertMessage:(NSString *)message{
    [[[UIAlertView alloc] initWithTitle:@"Alert!!"
                                message:message
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:@"Cancel", nil] show];
}

-(void)continueLogin
{
    
    
    
    //5
    [loginManager getSSOAppSessionOnCompletion:^(SSOSession * _Nullable session, NSError * _Nullable error)
     {
         if (error!=NULL)
         {
             return ;
         }
         
         
         if (!session.type)
         {
             [loginManager getSSOGlobalSessionWithUserDataEnabled:true completion:^(SSOSession * _Nullable session, NSError * _Nullable error)
              {
                  if (error!=NULL)
                  {
                      if (error.code == 4000) //SDK not initialized
                      {
                          [self showAlertMessage:error.localizedDescription];
                      }
                      else
                      {
                          // do passive login
                          crossapp = 2;
                          [self showAlertMessage:[NSString stringWithFormat: @"Continue with Facebook ?"]];
                      }
                      
                      return ;
                  }
                  crossapp = 1;
                  if([session.type isEqualToString: @"sso" ])
                  {
                      NSString * userId = session.identifier;
                      if([userId length] == 0)
                      {
                          // migrated user
                          userId = @"sso";
                      }
                      [self showAlertMessage:[NSString stringWithFormat: @"Global login with %@ ? ",userId]];
                  }
                  else
                  { //type: googleplus or facebook
                      [self showAlertMessage:[NSString stringWithFormat: @"Global login with %@ ?",session.type]];
                  }
                  
              }];
             
         }
         
     }];
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    // the user clicked OK
    if (buttonIndex == 0)
    {
        if(crossapp == 1)
        {
            crossapp = 0;
            [loginManager copySSOGlobalSessionToAppOnCompletion:^(NSDictionary *info, NSError *error) {
                if (error == NULL) {
                    [self showAlertMessage:error.localizedDescription];
                }
            }];
        }
        if (crossapp == 2)
        {
            crossapp = 0;
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [[NSSOSocialLoginManager sharedManager] doPassiveLoginOnCompletion:^(NSDictionary *info, NSError *err)
             {
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 if (err != NULL)
                 {
                     [self showAlertMessage:err.localizedDescription];
                 }
                 else
                 {
                     [self showAlertMessage:@"Automatically Loged in with your Facebook"];
                 }
             }];
        }
    }
    if(buttonIndex == 1)
    {
        // Try manulal login
    }
}


@end
