//
//  NSSOLoginViewController.m
//  NativeSSOLogin
//
//  Created by Pankaj Verma on 25/10/16.
//  Copyright © 2016 Pankaj Verma. All rights reserved.
//

#import "NSSOLoginViewController.h"
#import "NSSOCrossAppLoginManager.h"
#import "NSSOSocialLoginManager.h"
#import "NSSOTempGlobal.h"
#import <linkedin-sdk/LISDK.h>

@interface NSSOLoginViewController ()<GoogleLoginDelegate,TruecallerLoginDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailOrMobile;

@property (weak, nonatomic) IBOutlet UITextField *otpOrPassword;

@end

@implementation NSSOLoginViewController
UIAlertView *alertView1;
- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendOtpOnEmail:(id)sender {
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] sendLoginOtpOnEmail:_emailOrMobile.text mobile:@"" success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on Email: %@",_emailOrMobile.text]];
    } failure:^(NSError * _Nullable error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}

- (IBAction)sendOtpOnMobile:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] sendLoginOtpOnEmail:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on Mobile: %@",_emailOrMobile.text]];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}

- (IBAction)resendOtpOnEmail:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] resendLoginOTPOnEmail:_emailOrMobile.text mobile:@"" success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on Email: %@",_emailOrMobile.text]];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
    
}
- (IBAction)resendOtpOnMobile:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] resendLoginOTPOnEmail:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:[NSString stringWithFormat:@"otp sent on Mobile: %@",_emailOrMobile.text]];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

- (IBAction)signInWithEmail:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[[NSSOCrossAppLoginManager alloc] init] verifyLoginOtpPassword:_otpOrPassword.text email:_emailOrMobile.text mobile:@"" success:^{
        [self showAlertMessage:@"Login successful"];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

- (IBAction)signInWithMobile:(id)sender
{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[[NSSOCrossAppLoginManager alloc] init] verifyLoginOtpPassword:_otpOrPassword.text email:@"" mobile:_emailOrMobile.text success:^{
        [self showAlertMessage:@"Login successful"];
    } failure:^(NSError *error) {
        [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
    }];
}

- (IBAction)googleSignIn:(id)sender
{
    [[NSSOSocialLoginManager sharedManager] loginToGoogleOnViewController:self];
}

- (IBAction)truecallerSignIn:(id)sender
{
    [[NSSOSocialLoginManager sharedManager] loginToTruecallerOnViewController:self];
}

- (IBAction)facebookSignIn:(id)sender {
      [[NSSOSocialLoginManager sharedManager] loginToFacebookOnViewController:self completion:^(NSMutableDictionary *info, NSError *error) {
        if (error == NULL)
        {
            if (deprecatedAllowed)
            {
                [[NSSOCrossAppLoginManager sharedLoginManager] loginUsingSocialInfo:info  success:^{
                    [self showAlertMessage:@"Login successful"];
                } failure:^(NSError *error) {
                    [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                }];
            }
            else
            {
                            SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
                            si.accessToken = [info valueForKey:@"accessToken"];
                            si.oauthId = [info valueForKey:@"oauthId"];
                            si.user_mobile_phone = false;
                            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOFacebookLogin
                                                                                 usingSocialInfo:si
                                                                                         success:^{
                                                                                             [self showAlertMessage:@"Login successful"];
                
                                                                                         }
                                                                                         failure:^(SSOError * _Nullable error)
                             {
                                [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                         }];

            }
            
        }
        else{
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }
    }];
}

- (IBAction)gsmaSignIn:(id)sender
{
    SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
    si.phoneNumber = _emailOrMobile.text;
    si.gsmaToken = _otpOrPassword.text;
    [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOGsmaLogin
                                                         usingSocialInfo:si
                                                                 success:^{
                                                                     [self showAlertMessage:@"Login successful"];
                                                                     
                                                                 }
                                                                 failure:^(SSOError * _Nullable error)
     {
         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
     }];
}
- (IBAction)linkedinLogin:(id)sender
{
    SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
//    si.oauthId = _emailOrMobile.text;
//    si.accessToken = _otpOrPassword.text;
    
        si.oauthId = @"AAoAABtgYWoBvw5QlpJnZ-p02UEnEKuljBeaUuI";
//        si.accessToken = @"AQXMpfcbIXUDhb660X39mflgGClWi6Ff1ymfbUdK8iqtJ5QsJCucbz7qj_KH02GegFgkK3UadLmgME02dbNrhH04exUbmU7y4Ivov3AW-FTxTadQQGLzXxOaqcYvzsy9fPrzMVtJV0tDvJhmq47u8EuFAhTKovgs1gpnztktefMbSXKFqzuEat853J2w9W10h4nnyUsxQB-HiILiVRayXWYwQOSqvxHRvECk7-b0xaiXkkIAH3lftHroZBsYYc3xG7bjtgqQiBodji2vdRsxWkDfctBLuwzBDD6s-2SzE0R4Ilsr3Rk7pngvUitIvFo49mn3wzofbD3AwY23n7UYThM1b29JVQ";

    
    
    
    
//
//            si.oauthId = @"AAoAAAhIPfkBFoF4UFf3ciC51M5qR3jDklrNRE0";
//            si.accessToken = @"AQX0IY_CpWaBM3sZoqKSXWHKrm2DDpAkE470l_4HaZY7Q3bd-36KAuPHA1Z18dnTpM9R6jWYWR7TD-16g6RbhZZ--Y4B4QNhks8J2uHGbQZ0TNAfXg2wSOAGqU8FFLbh7FhhrSvLzT6ibM0-wL5PO8KrvK4WrddVGMWctwATCrJ8WDgPHtR8CFytQFKWK40HKe3Kvj5Dr5onfV1cM7JlqpI8Omcbp-O0p9EY6EW_KZKKO-YPccq0i88MS1o35wsIZckRHsmLkS2EWQnSCJg9F5iraf88FJCHxzVC560EBK-gGlVgf9mFVKAvnkppM6ikddx9-_4vfWvX2RYzrBbxO0o3gbXbcQ";
//
//outhId:
//    AAoAAAhIPfkBFoF4UFf3ciC51M5qR3jDklrNRE0
//
//accessToken:
//    AQX0IY_CpWaBM3sZoqKSXWHKrm2DDpAkE470l_4HaZY7Q3bd-36KAuPHA1Z18dnTpM9R6jWYWR7TD-16g6RbhZZ--Y4B4QNhks8J2uHGbQZ0TNAfXg2wSOAGqU8FFLbh7FhhrSvLzT6ibM0-wL5PO8KrvK4WrddVGMWctwATCrJ8WDgPHtR8CFytQFKWK40HKe3Kvj5Dr5onfV1cM7JlqpI8Omcbp-O0p9EY6EW_KZKKO-YPccq0i88MS1o35wsIZckRHsmLkS2EWQnSCJg9F5iraf88FJCHxzVC560EBK-gGlVgf9mFVKAvnkppM6ikddx9-_4vfWvX2RYzrBbxO0o3gbXbcQ
//
//
    
    NSArray *permissions = [NSArray arrayWithObjects:LISDK_EMAILADDRESS_PERMISSION, nil];
    [LISDKSessionManager createSessionWithAuth:permissions state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *returnState)
    {
        NSLog(@"%s","success called!");
        LISDKSession *lsession = [[LISDKSessionManager sharedInstance] session];
        NSLog(@"Session : %@", lsession.description);
        NSLog(@"AccessToken : %@", lsession.accessToken.accessTokenValue);
        si.accessToken = lsession.accessToken.accessTokenValue;
            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOLinkedinLogin
                                                                 usingSocialInfo:si
                                                                         success:^{
                                                                             [self showAlertMessage:@"Login successful"];

                                                                         }
                                                                         failure:^(SSOError * _Nullable error)
             {
                 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
             }];
        
//        [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~"
//                                            success:^(LISDKAPIResponse *response)
//         {
//             NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
//             NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//             NSLog(@"Authenticated user name : %@ %@", [dictResponse valueForKey: @"firstName"], [dictResponse valueForKey: @"lastName"]);
//         } error:^(LISDKAPIError *apiError)
//         {
//             NSLog(@"Error : %@", apiError);
//         }];
//
        
        
    } errorBlock:^(NSError *error)
    {
        NSLog(@"%s: %@","error called!", error.description);
    }];
    

    
    
}


#pragma Sign in via Google delegate methods

-(void) googleSignInSuccessfulWithInfo:(NSDictionary *)info
{
    if (deprecatedAllowed)
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] loginUsingSocialInfo:info  success:^{
            [self showAlertMessage:@"Login successful"];
        } failure:^(NSError *error) {
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }];
    }
    else
    {
            SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
            si.accessToken = [info valueForKey:@"accessToken"];
            si.oauthId = [info valueForKey:@"oauthId"];
            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOGoogleLogin
                                                                 usingSocialInfo:si
                                                                         success:^{
                                                                             [self showAlertMessage:@"Login successful"];
        
                                                                         }
                                                                         failure:^(SSOError * _Nullable error)
             {
                 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
             }];

    }
    
    

}
-(void) googleSignInFailWithError:(NSError *) error{
 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
}
#pragma Sign in via Google delegate methods

-(void) truecallerSignInSuccessfulWithInfo:(NSDictionary *)info
{
    if (deprecatedAllowed)
    {
        [[NSSOCrossAppLoginManager sharedLoginManager] loginUsingSocialInfo:info  success:^{
            [self showAlertMessage:@"Login successful"];
        } failure:^(NSError *error) {
            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
        }];
    }
    else
    {
            SSOSocialInfo *si = [[SSOSocialInfo alloc] init];
            si.payload = [info valueForKey:@"payload"];
            si.signature = [info valueForKey:@"signature"];
            [[NSSOCrossAppLoginManager sharedLoginManager] performSocialActivity:SSOTruecallerLogin
                                                                 usingSocialInfo:si
                                                                         success:^{
                                                                             [self showAlertMessage:@"Login successful"];
        
                                                                         }
                                                                         failure:^(SSOError * _Nullable error)
             {
                 [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
             }];

    }
   
    

    
}
-(void) truecallerSignInFailWithError:(NSError *) error{
    [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
}
- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)showAlertMessage:(NSString *)message{
    if (!alertView1){
        alertView1 = [[UIAlertView alloc] init];
    }
    alertView1.title = @"Alert!";
    alertView1.message = message;
    alertView1.delegate = self;
    [alertView1 addButtonWithTitle:@"Ok"];
    [alertView1 show];
//        [[[UIAlertView alloc] initWithTitle:@"Alert!!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

#pragma Add email and mobile

-(IBAction)addAlternateEmail:(id)sender{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] addAlternateEmail:_emailOrMobile.text
                                                             success:^{
                                                                [self showAlertMessage:@"OTP sent on Email"];
                                                             } failure:^(NSError *error) {
                                                                  [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                                                             }];
}
-(IBAction)updateMobile:(id)sender{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] updateMobile:_emailOrMobile.text
                                                        success:^{
                                                            [self showAlertMessage:@"OTP sent on Mobile"];
                                                        } failure:^(NSError *error) {
                                                            [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                                                        }];
}
-(IBAction)verifyAddAlternateEmailOtp:(id)sender{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] verifyAddAlternateEmailOtp:_otpOrPassword.text
                                                                     forEmail: _emailOrMobile.text
                                                                      success:^{
                                                                         [self showAlertMessage:@"Email added  successfully"];
                                                                     } failure:^(NSError *error) {
                                                                         [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                                                                     }];
}
-(IBAction)verifyUpdateMobileOtp:(id)sender{
    [[NSSOCrossAppLoginManager sharedLoginManager] setTrakingChannel:@"timesprime"];
    [[NSSOCrossAppLoginManager sharedLoginManager] verifyUpdateMobileOtp:_otpOrPassword.text
                                                               forMobile:_emailOrMobile.text
                                                                 success:^{
                                                                     [self showAlertMessage:@"Mobile updated  successfully"];
                                                                 } failure:^(NSError *error) {
                                                                     [self showAlertMessage:[NSString stringWithFormat:@"%ld:%@",(long)error.code,error.localizedDescription]];
                                                                 }];
}

@end
