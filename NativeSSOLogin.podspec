#
# Be sure to run `pod lib lint NativeSSOLogin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'NativeSSOLogin'
  s.version          = '1.1.5'
s.summary          = 'This pod support cross app login i.e. single sign on. '
  s.description      ='This pod provide Native Login .This pod is for iOS Apps of Times Internet Limited which sign in is provided and handled by AGI SSO Team. Apps belonging to the same team support cross app login i.e. login in one app will do login in all other apps of the family/team and logout from one will logout from all other.'

  s.homepage         = 'https://bitbucket.org/agi_sso/nativessologin'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pankaj Verma' => 'pankaj.verma@timesinternet.in' }
  s.source           = { :git => 'https://bitbucket.org/agi_sso/nativessologin.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'NativeSSOLogin/Classes/**/*'
  
  # s.resource_bundles = {
  #   'NativeSSOLogin' => ['NativeSSOLogin/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'

s.frameworks = 'Accounts', 'Social', 'Foundation'
# Uncomment below if you want to include static Google framework and Facebook and Truecaller 
#s.dependency 'FBSDKCoreKit'
#s.dependency 'FBSDKLoginKit'
#s.dependency 'Google/SignIn'
#s.dependency 'TrueSDK'
end
