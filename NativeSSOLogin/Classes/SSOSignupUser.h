//
//  SSOSignupUser.h
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOSignupUser : NSObject

@property (nonatomic, nullable, readonly) NSString *name;
@property (nonatomic, nullable, readonly) NSString *mobile;
@property (nonatomic, nullable, readonly) NSString *email;
@property (nonatomic, nullable, readonly) NSString *gender;
@property (nonatomic, readonly) NSString *password;
@property (nonatomic, nullable, readonly) NSString *otp;
@property (nonatomic, nullable, readonly) NSString *ssoid;
@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString *timespointsPolicy;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

-(void)setName:(NSString * _Nullable)name;
-(void)setMobile:(NSString * _Nullable)mobile;
-(void)setEmail:(NSString * _Nullable)email;
-(void)setGender:(NSString * _Nullable)gender;
-(void)setPassword:(NSString * _Nullable)password;
-(void)setOtp:(NSString * _Nullable)otp;
-(void)setSsoid:(NSString * _Nullable)ssoid;
@end
