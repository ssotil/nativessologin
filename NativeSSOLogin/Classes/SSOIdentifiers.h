//
//  SSOIdentifiers.h
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOIdentifiers : NSObject
@property (nonatomic, nullable) NSString *identifier;
@property (nonatomic, nullable) NSString *email;
@property (nonatomic, nullable) NSString *mobile;
@property (nonatomic, nullable) NSString *password;
@property (nonatomic, nullable) NSString *otp;
@property (nonatomic, nullable) NSString *countryCode;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
