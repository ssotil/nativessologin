//
//  SSOIdentifiers.m
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import "SSOIdentifiers.h"

@implementation SSOIdentifiers
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary
{
    if (self = [super init])
    {
        if (self != nil) {
            if (![dictionary isKindOfClass:[NSNull class]])
            {
                [self setValuesForKeysWithDictionary:dictionary];
            }
        }
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"undefined key  = %@",key);
}

@end
