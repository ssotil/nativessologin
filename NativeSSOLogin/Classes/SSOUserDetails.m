//
//  SSOUserDetails.m
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import "SSOUserDetails.h"

@implementation SSOUserDetails
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary
{
    if (self = [super init])
    {
        if (self != nil) {
            if (![dictionary isKindOfClass:[NSNull class]])
            {
                [self setValuesForKeysWithDictionary:dictionary];
            }
        }
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"undefined key  = %@",key);
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.firstName forKey:@"firstName"];
    [encoder encodeObject:self.lastName forKey:@"lastName"];
    [encoder encodeObject:self.gender forKey:@"gender"];
    [encoder encodeObject:self.dob forKey:@"dob"];
    [encoder encodeObject:self.city forKey:@"city"];
    [encoder encodeObject:self.ssoid forKey:@"ssoid"];
    [encoder encodeObject:self.dp forKey:@"dp"];
    [encoder encodeObject:self.fbConnected forKey:@"fbConnected"];
    [encoder encodeObject:self.gpConnected forKey:@"gpConnected"];
    [encoder encodeObject:self.passwordExists forKey:@"passwordExists"];
    [encoder encodeObject:self.primaryEmail forKey:@"primaryEmail"];
    [encoder encodeObject:self.primeProfile forKey:@"primeProfile"];
    [encoder encodeObject:self.mobileList forKey:@"mobileList"];
    [encoder encodeObject:self.emailList forKey:@"emailList"];
    [encoder encodeObject:self.emailList forKey:@"mobileData"];
    [encoder encodeObject:self.shareDataAllowed forKey:@"shareDataAllowed"];
    [encoder encodeObject:self.termsAccepted forKey:@"termsAccepted"];
    [encoder encodeObject:self.timespointsPolicy forKey:@"timespointsPolicy"];
    [encoder encodeObject:self.isEuUser forKey:@"isEuUser"];
 
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init])) {
        self.firstName =  [decoder decodeObjectForKey:@"firstName"];
        self.lastName =  [decoder decodeObjectForKey:@"lastName"];
        self.gender =  [decoder decodeObjectForKey:@"gender"];
        self.dob =  [decoder decodeObjectForKey:@"dob"];
        self.city =  [decoder decodeObjectForKey:@"city"];
        self.ssoid =  [decoder decodeObjectForKey:@"ssoid"];
        self.dp =  [decoder decodeObjectForKey:@"dp"];
        self.fbConnected =  [decoder decodeObjectForKey:@"fbConnected"];
        self.gpConnected =  [decoder decodeObjectForKey:@"gpConnected"];
        self.passwordExists =  [decoder decodeObjectForKey:@"passwordExists"];
        self.primaryEmail =  [decoder decodeObjectForKey:@"primaryEmail"];
        self.primeProfile =  [decoder decodeObjectForKey:@"primeProfile"];
        self.mobileList =  [decoder decodeObjectForKey:@"mobileList"];
        self.emailList =  [decoder decodeObjectForKey:@"emailList"];
        
        self.shareDataAllowed =  [decoder decodeObjectForKey:@"shareDataAllowed"];
        self.termsAccepted =  [decoder decodeObjectForKey:@"termsAccepted"];
        self.timespointsPolicy =  [decoder decodeObjectForKey:@"timespointsPolicy"];
        self.isEuUser =  [decoder decodeObjectForKey:@"isEuUser"];
    }
    return self;
}
@end
