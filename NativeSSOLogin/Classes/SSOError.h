//
//  SSOError.h
//  Pods
//
//  Created by Pankaj Verma on 06/04/17.
//
//

#import <Foundation/Foundation.h>
#define SSOSDKErrorDomain @"agi_sso"
#define SSOSDKErrorDescription NSLocalizedDescriptionKey

typedef NS_ENUM(NSUInteger, SSOSDKErrorCode)
{
    SSOSDKErrorCodeUnauthorizedAccess = 404,
    SSOSDKErrorCodeInvalidRequest = 413,
    SSOSDKErrorCodeSDKNotInitialized = 4000,
    SSOSDKErrorCodeMigrateUnverifiedUserError = 4011,
    SSOSDKErrorCodeDataNil = 4100,
    SSOSDKErrorCodeNotString,
    SSOSDKErrorCodeNotBoolean,
    SSOSDKErrorCodeSessionNotFound,
    SSOSDKErrorCodeOauthIdNotFound,
    SSOSDKErrorCodeOauthSiteIdNotFound,
    SSOSDKErrorCodeAccessTokenNotFound,
    SSOSDKErrorCodeFbLoginFlowCancelled,
    SSOSDKErrorCodePayloadNotFound,
    SSOSDKErrorCodeSignatureNotFound,
    SSOSDKErrorCodeResponseCodeNotFound,
    SSOSDKErrorCodeFailureInfoNotFound,
    SSOSDKErrorCodeAppIdentifierPrefixMissing, 
    SSOSDKErrorCodeChannelMissing,
    SSOSDKErrorCodeSiteIdMissing,
    SSOSDKErrorCodePicSizeExceeded,
    SSOSDKErrorCodeTermsInvalidString,
    SSOSDKErrorCodeShareDataInvalidString,
    SSOSDKErrorCodeTimespointPolicyInvalidString,
    SSOSDKErrorCodePasswordBlank


};

@interface SSOError : NSError
/*!
 * @brief Method for returning the error code for a given error object
 * @return SSOSDKErrorCode code
 */
- (SSOSDKErrorCode)getErrorCode;

/*!
 * @brief Method for creating an error object given an error code. Intended for internal usage.
 * @param code Error code
 * @return SSOError Error object
 */
+ (SSOError *)errorWithCode:(SSOSDKErrorCode)code;

/*!
 * @brief Method for creating an error object given an error code and description. Intended for internal usage.
 * @param code Error code
 * @param errorDescription Error description
 * @return TCError Error object
 */
+ (SSOError *)errorWithCode:(SSOSDKErrorCode)code
               description:(NSString *)errorDescription;
@end
