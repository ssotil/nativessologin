//
//  SSOSignupUser.m
//  Pods
//
//  Created by Pankaj Verma on 20/04/17.
//
//

#import "SSOSignupUser.h"

@implementation SSOSignupUser
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary
{
    if (self = [super init])
    {
        if (self != nil) {
            if (![dictionary isKindOfClass:[NSNull class]])
            {
                [self setValuesForKeysWithDictionary:dictionary];
            }
        }
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    NSLog(@"undefined key  = %@",key);
}

-(void)setName:(NSString * _Nullable)name
{
    _name = name;
}
-(void)setMobile:(NSString * _Nullable)mobile
{
    _mobile = mobile;
}
-(void)setEmail:(NSString * _Nullable)email
{
    _email = email;
}
-(void)setGender:(NSString * _Nullable)gender
{
    _gender = gender;
}
-(void)setPassword:(NSString * _Nullable)password
{
    _password = password;
}
-(void)setOtp:(NSString * _Nullable)otp
{
    _otp = otp;
}
-(void)setSsoid:(NSString * _Nullable)ssoid
{
    _ssoid = ssoid;
}

@end
