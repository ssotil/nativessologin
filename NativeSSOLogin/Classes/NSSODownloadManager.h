//
//  NSSODownloadManager.h
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import <Foundation/Foundation.h>
#import "SSOError.h"
typedef void (^privateCompletionBlock) (NSDictionary * _Nullable dataDictionary, SSOError * _Nullable error);



@interface NSSODownloadManager : NSObject
{
    
}
NS_ASSUME_NONNULL_BEGIN
//+ (id)ssoSharedManager;


-(void)downloadDataForUrlImageUpload:(NSString *)baseUrl
                               scope:(NSUInteger)scope
                                path:(NSString *)path
                             andBody:(NSMutableData*)body_data
                   completionHandler:(privateCompletionBlock)completionHandler;

-(void)downloadDataForBaseUrl:(NSString *)baseUrl
                        scope:(NSUInteger)scope
                         path:(NSString *)path
                       params:(NSDictionary * _Nullable )params
                      queries:(NSDictionary * _Nullable )queries
            completionHandler:(privateCompletionBlock)completionHandler;
NS_ASSUME_NONNULL_END
@end
