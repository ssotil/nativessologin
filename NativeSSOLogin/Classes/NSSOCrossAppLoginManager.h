//
//  NSSOCrossAppLoginManager.h
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//


#import <UIKit/UIKit.h>
#import "SSOSession.h"
#import "SSOBasicUserDetails.h"
#import "SSOUserStatus.h"
#import "SSOUserDetails.h"
#import "SSOUserUpdates.h"
#import "SSOError.h"
#import "SSOSocialInfo.h"
#import "SSOSignupUser.h"
#import "GdprDetails.h"
typedef NS_OPTIONS(NSUInteger, SSOSocialActivityOptions)
{
    SSOFacebookLogin = (1UL << 0), 
    SSOGoogleLogin = (1UL << 1),
    SSOTruecallerLogin = (1UL << 2),
    SSOFacebookLink = (1UL << 3),
    SSOGoogleLink = (1UL << 4),
    SSOFacebookPicUpload = (1UL << 5),
    SSOGooglePicUpload = (1UL << 6),
    SSOFacebookDelink = (1UL << 7),
    SSOGoogleDelink = (1UL << 8),
    SSOGsmaLogin =  (1UL << 9),
    SSOLinkedinLogin = (1UL << 10)
} NS_ENUM_AVAILABLE(10_7, 5_0);

typedef NS_OPTIONS(NSUInteger, SSOSignupOptions)
{
    SSOFullSignup = (1UL << 0),
    SSOOnlyMobileSignup = (1UL << 1)
} NS_ENUM_AVAILABLE(10_7, 5_0);

typedef NS_OPTIONS(NSUInteger, SSOPickUploadOptions)
{
    SSOCamera = (1UL << 0), 
    SSOPhotoGallery = (1UL << 1),
    SSODefault = (1UL << 2)
} NS_ENUM_AVAILABLE(10_7, 5_0);

typedef void (^errorBlock) (SSOError *  _Nullable error);
typedef void (^voidBlock) (void);
typedef void (^successBlock) (NSDictionary * _Nullable info);
typedef void (^completionBlock) (NSDictionary * _Nullable info,SSOError * _Nullable error);
typedef void (^sessionBlock) (SSOSession * _Nullable  session, SSOError * _Nullable error);
typedef void (^userStatusBlock) (SSOUserStatus * _Nullable user);
typedef void (^userDetailsBlock) (SSOUserDetails * _Nullable user,SSOError * _Nullable error);
typedef void (^userUpdatesBlock) (SSOUserUpdates * _Nullable user);
typedef void (^imageUploadStart) (void);

@interface NSSOCrossAppLoginManager : NSObject
{
    
}
NS_ASSUME_NONNULL_BEGIN
@property (nonatomic) NSString *nssoBaseUrl;
@property (nonatomic) NSString *nssoMSocialBaseUrl;

@property (nonatomic) NSString * trakingChannel;
+ (id) sharedLoginManager;

//SDK initialization
/*
 Each App has its channel and corresponding site id. SSO functionalitis will not work without these two.
 Team id(get it from member center) is used for keychain sharing. Global session will not work if teamId is incorrect. 
 If your development team id and production team id are not same, carefully  change them accordingly.
 */
//-(void)ssoSetupForChannel:(NSString *)channel
//                   siteId:(NSString *)siteId
//                   teamId:(NSString *)teamId
//               completion:(completionBlock)completion;

-(void)ssoSetupForChannel:(NSString *)channel
                   siteId:(NSString *)siteId
                   teamId:(NSString *)teamId
                   isLive:(Boolean)isLive
               completion:(completionBlock)completion;

//Social Login : Login via Facebook, Google or Truecaller
/*
 FB/Google: Get oauthId and accessToken from FB/Google and pass in bellow method.
 Truecaller: Get payload and signature from truecaller and pass in bellow method.
*/

-(void)loginUsingSocialInfo:(NSDictionary *)info
                    success:(voidBlock)success
                    failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

#pragma mark Login via email or mobile
/*
 User can login via their registered email or mobile.
 If user know their password they can login with registered email/mobile (by calling verifyLoginOtpPassword:) or
 they can request a login OTP on one of their registered email/mobile.
 */

-(void)sendLoginOtpOnEmail:(NSString *)email
                    mobile:(NSString *)mobile
                   success:(voidBlock)success
                   failure:(errorBlock)failure;

-(void)resendLoginOTPOnEmail:(NSString *)email
                      mobile:(NSString *)mobile
                     success:(voidBlock)success
                     failure:(errorBlock)failure __deprecated;

-(void)verifyLoginOtpPassword:(NSString *)password
                        email:(NSString *)email
                       mobile:(NSString *)mobile
                      success:(voidBlock)success
                      failure:(errorBlock)failure;

#pragma mark New user registration or signup
/*
 User can provide either mobile or email or both.
 User will provede their gender, choose a password(following standerd) and agree to send offer notifications(YES or NO).
 All fields are mandatory except one of email or mobile can be blank.
 A sign up otp is preferably send on mobile.
 If mobile is not provided otp will be send on email.
 User can resend OTP.
 User will be logged in once OTP is successfully verified.
 */
-(void)registerUser:(NSString *)name
             mobile:(NSString *)mobile
              email:(NSString *)email
           password:(NSString *)password
             gender:(NSString *)gender
 isSendOfferEnabled:(BOOL)isSendOfferEnabled
            success:(voidBlock)success
            failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSignupActivity :_ forUser:_ success:_ failure:_")));

-(void)resendSignUpOtpForEmail:(NSString *)email
                        mobile:(NSString *)mobile
                       success:(voidBlock)success
                       failure:(errorBlock)failure;

-(void)verfiySignUpOTP:(NSString *)otp
                 email:(NSString *)email
                mobile:(NSString *)mobile
               success:(voidBlock)success
               failure:(errorBlock)failure;

#pragma mark signOutUser
/*
 User will be loged out.
 Logging out will delete App session. Login type and identifier will not be deleted from App session.
 Global session will be deleted only if it is same as App session.
 */
-(void)signOutUser:(voidBlock)success
           failure:(errorBlock)failure;

/*
 User will be blocked for the business(channel).
 */

 -(void)blockUser:(voidBlock)success
 failure:(errorBlock)failure;

-(void)signOutUser:(voidBlock)success
           failure:(errorBlock)failure;
#pragma mark getUserDetails
/*
 User details ccontains date of birth(dob), profile pic url(dp), user's registered email list(emailList),First Name, Last Name, Gender, user's registered email list(mobileList), primary email, ssoId and whether Facebbok or Google plus account is connected with SSO or not.
 */

-(void)getUserDetails:(void(^)(NSDictionary *info))success
              failure:(void(^)(NSError * _Nullable error))failure
__attribute__((deprecated("new available method is, getUserDetailsOnCompletion")));


#pragma mark change password
/*
 User can change their password.
 New password must be different from previous three passwords.
 */
-(void)changePassword:(NSString *)oldPassword
          newPassword:(NSString *)newPassword
      confirmPassword:(NSString *)confirmPassword
              success:(voidBlock)success
              failure:(errorBlock)failure;

#pragma mark validate password
-(void)validatePassword:(NSString *)password
        confirmPassword:(NSString *)confirmPassword
                  email:(NSString *)email
                 mobile:(NSString *)mobile
                success:(voidBlock)success
                failure:(errorBlock)failure;

#pragma mark Forgot Password
/* If user can request for forgot password OTP on one of their registered email/mobile and with this OTP he/she can provide/create new password.
 New password must not match previous three passwords.
 */

-(void)getForgotPasswordOTPForEmail:(NSString *)email
                             mobile:(NSString *)mobile
                            success:(voidBlock)success
                            failure:(errorBlock)failure ;

-(void)resendForgotPasswordOTPForEmail:(NSString *)email
                                mobile:(NSString *)mobile
                               success:(voidBlock)success
                               failure:(errorBlock)failure;

-(void)verifyForgotPasswordForEmail:(NSString *)email
                             mobile:(NSString *)mobile
                                otp:(NSString *)otp
                           password:(NSString *)password
                    confirmPassword:(NSString *)confirmPassword
                            success:(voidBlock)success
                            failure:(errorBlock)failure;


#pragma mark Add Email or Mobile
/*
 A SSO user can have maximum 3 emails and one mobile.
 */
-(void)addAlternateEmail:(NSString *)email
                 success:(voidBlock)success
                 failure:(errorBlock)failure;

-(void)verifyAddAlternateEmailOtp:(NSString *)otp
                         forEmail:(NSString *)email
                          success:(voidBlock)success
                          failure:(errorBlock)failure ;

-(void)updateMobile: (NSString *) mobile
            success:(voidBlock)success
            failure:(errorBlock)failure;

-(void)verifyUpdateMobileOtp:(NSString *)otp
                   forMobile:(NSString *)mobile
                     success:(voidBlock)success
                     failure:(errorBlock)failure;

//MARK:- Login Sessions
/*
 Apps which are using their own login have to migrate their login session to SSO session so that users which are already login in App will also be in login state after update(integration of NativeSSO SDK).
 This is one time call.
 After migration done App can remove its own session.
 */
-(void)migrateCurrentSessionToAppHavingTicketId:(NSString *)ticketId
                                     completion:(completionBlock)completion;


-(void)getGlobalSessionOnCompletion:(completionBlock)completion
__attribute__((deprecated("new available method is,getGlobalSessionWithUserDataEnabled:_ completion:_")));



-(void)getAppSessionOnCompletion:(completionBlock)completion
__attribute__((deprecated("new available method is,getSSOAppSessionOnCompletion:_")));

/*
 This is Cross app login and works silentely.
 If User is new and Global session exist, he/she can contine with global session.
 Global session will be copied to App with new refreshed ticketId.
 Note: Existing user who are in logout state before integration of This SDK in the App will be treated as new user and will be (may be )silentely login after this update. They are requested to logout if not happy or want to login with different account.
 */
-(void)copySSOGlobalSessionToAppOnCompletion:(completionBlock)completion
;

//MARK:- Renew ticket
/*
 This API will reset the life time of ticket to 30 days from now of the log in user .
 */
-(void )renewTicket:(void(^)(void))success
            failure:(errorBlock)failure;

// Update user details
/*
 User can update their name, dob and gender. The field which you do not want to update leave them blank(empty string). On success updated details will be returned.
 */

-(void)updateFirstName:(NSString *)firstName
              lastName:(NSString *)lastName
                   dob:(NSString *)dob
                gender:(NSString *)gender
                success:(voidBlock)success
               failure:(errorBlock)failure
__attribute__((deprecated("new available method is, updateUserDetails:_ success:_ failure:_")));

//v2.1
// Social link delink
/*
  Get oauthId and accessToken fron FB/GP and pass in bellow function.
 */
-(void)linkSocialAccountUsingInfo:(NSDictionary *)info
                          success:(voidBlock)success
                          failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

-(void)delinkFacebook:(voidBlock)success
              failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

-(void)delinkGoogleplus:(voidBlock)success
                failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

// socialImageUpload
/*
 Get oauthId and accessToken fron FB/GP and pass in bellow function.
 */

-(void)uploadProfilePicFromSocialUsingInfo:(NSDictionary *)info
                                   success:(voidBlock)success
                                   failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performSocialActivity:_ usingSocialInfo:_ success:_ failure:_")));

#pragma mark - Upload Profile Pic
//upload pic from camera or gallery
-(void)openPhotoSelectorOnViewController:(UIViewController *)vc
                                 success:(voidBlock)success
                                 failure:(errorBlock)failure
__attribute__((deprecated("new available method is, performPickUploadActivity:_ onController:_ startRequest:_ success:_ failure:_")));

#pragma mark - New Apis 2.2

//getUserDetails: user details will be returned in an object of type SSOUserDetails.
-(void)getUserDetailsOnCompletion:(userDetailsBlock)completion;
-(SSOUserDetails *)getUserDetailsLocal;
// This Api will check if user(email/mobile) is registered with SSO). Status will be returned in an object of type SSOUserStatus.
-(void)getStatusForIdentifier:(NSString *)identifier
                      success:(userStatusBlock)success
                      failure:(errorBlock)failure;

//update user details : first name, last name, gender(M or F), dob,city. Leave the field blank which you do not want to update.
// Updated details will be returned in an object of type SSOUserUpdates
-(void)updateUserDetails:(SSOUserUpdates *)userDetails
                 success:(userUpdatesBlock)success
                 failure:(errorBlock)failure;


//Login Sessions

//This will create app session for unverified users. Session will be saved only in App session not in Global session.
-(void) createAppSessionForTicketId:(NSString *)ticketId completion:(completionBlock)completion;

//global session:  if userDataEnabled is true, it will return session + basic info of global user else only global session will be returnd. result will be an object of type SSOSession
-(void)getSSOGlobalSessionWithUserDataEnabled:(Boolean)userDataEnabled
                                   completion:(sessionBlock)completion;

//app session: result will be an object of type SSOSession
-(void)getSSOAppSessionOnCompletion:(sessionBlock)completion;

// Login via Facebook,Google or Truecaller; link/d-link Facebook or Google account to SSO; upload profile pic from Gggole or Facebook account
//Note: Truecaller login is in beta phase
//For facebook and Google set oauthId and accessToken
//For Trucaller(only login supported) set payload and signature

-(void)performSocialActivity:(SSOSocialActivityOptions)option
             usingSocialInfo:(SSOSocialInfo * _Nullable)info
                     success:(voidBlock)success
                     failure:(errorBlock)failure;

//user signup: User can signup by providing their full details or just mobile number and name.
-(void)performSignupActivity:(SSOSignupOptions)options
                     forUser:(SSOSignupUser *)user
                     success:(voidBlock)success
                     failure:(errorBlock)failure;

//profile pic upload from camera/gallery
-(void)performPickUploadActivity:(SSOPickUploadOptions)options
                    onController:(UIViewController *)vc
                    startRequest:(imageUploadStart) uploadStart
                         success:(successBlock)success
                         failure:(errorBlock)failure;
//Delete user account
//-(void)deleteUserAccountWithPassword:(NSString *)password onCompletion:(completionBlock)completion;


-(void)loginWithGdprDetails:(GdprDetails *)gdprDetails
                    success:(voidBlock)success
                    failure:(errorBlock)failure;

NS_ASSUME_NONNULL_END
@end
