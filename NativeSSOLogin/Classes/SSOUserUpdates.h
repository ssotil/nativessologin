//
//  SSOUserUpdates.h
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOUserUpdates : NSObject
@property (nonatomic, nullable) NSString *firstName;
@property (nonatomic, nullable) NSString *lastName;
@property (nonatomic, nullable) NSString * dob;
@property (nonatomic, nullable) NSString * gender;
@property (nonatomic, nullable) NSString * city;
@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString * timespointsPolicy;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
@end
