//
//  SSOUserDetails.h
//  Pods
//
//  Created by Pankaj Verma on 05/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOUserDetails : NSObject<NSCoding>
@property (nonatomic, nullable) NSString *firstName;
@property (nonatomic, nullable) NSString *lastName;
@property (nonatomic, nullable) NSString * gender;
@property (nonatomic, nullable) NSString * dob;
@property (nonatomic, nullable) NSString * city;

@property (nonatomic, nullable) NSString * ssoid;
@property (nonatomic, nullable) NSString * dp;
@property (nonatomic, nullable) NSDictionary *mobileList;
@property (nonatomic, nullable) NSDictionary *emailList;
@property (nonatomic, nullable) NSDictionary *mobileData;
@property (nonatomic, nullable) NSString * primaryEmail;
@property (nonatomic, nullable) NSString * primeProfile;

@property (nonatomic, nullable) NSNumber * fbConnected;
@property (nonatomic, nullable) NSNumber * gpConnected;
@property (nonatomic, nullable) NSNumber * passwordExists;

@property (nonatomic, nullable) NSString * shareDataAllowed;
@property (nonatomic, nullable) NSString * termsAccepted;
@property (nonatomic, nullable) NSString * timespointsPolicy;
@property (nonatomic, nullable) NSString * isEuUser;
- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

@end
