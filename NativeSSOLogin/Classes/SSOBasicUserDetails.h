//
//  SSOUser.h
//  Pods
//
//  Created by Pankaj Verma on 04/04/17.
//
//

#import <Foundation/Foundation.h>

@interface SSOBasicUserDetails : NSObject
@property (nonatomic, nullable) NSString *firstName;
@property (nonatomic, nullable) NSString *lastName;
@property (nonatomic, nullable) NSString *email;
@property (nonatomic, nullable) NSString *mobile;
@property (nonatomic, nullable) NSString *countryCode;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;
@end
