//
//  NSSOUrlManager.m
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import "NSSOUrlRequestManager.h"
#import "NSSOGlobal.h"
#import "SSOKeychain.h"

NSString * const SSO_CHANNEL_KEY = @"channel";
NSString * const SSO_APP_VERSION_KEY = @"appVersion";
NSString * const SSO_PLATFORM_KEY = @"platform";
NSString * const SSO_GET_DATA_KEY = @"getData";


NSString * const SSO_TGID_KEY = @"tgId";
NSString * const SSO_SSEC_KEY = @"ssec";
NSString * const SSO_TICKETID_KEY = @"ticketId";

@implementation NSSOUrlRequestManager

+(NSMutableURLRequest *)getUrlRequestForBaseUrl:(NSString *)baseUrl
                                          scope:(NSUInteger)scope
                                           path:(NSString *)path
                                     BodyParams:(NSDictionary * _Nullable )params
                                        queries:(NSDictionary * _Nullable )queries
{
    NSString * ssoNativeApiPath = @"/sso/crossapp/identity/native/";
    NSMutableDictionary *sso_header_params;
    switch (scope)
    {
        case ssoGlobal:
            sso_header_params = [self getHeadersForSession:globalSession];
            [sso_header_params setValue:appHeader.getData forKey: SSO_GET_DATA_KEY];
            break;
        case ssomSocial:
            ssoNativeApiPath = @"";
            break;
        case ssoImageUpload:
            sso_header_params = [self getHeadersForSession:appSession];
            break;
        case ssoBase:
            sso_header_params = [self getHeadersForSession:appSession];
            break;
        case syncPixel:
            ssoNativeApiPath = @"/uid/";
            sso_header_params = [self getSyncHeaders];
            break;
        default:
            sso_header_params = nil;
            break;
    }
    NSString *fullPath = [NSString stringWithFormat:@"%@%@",ssoNativeApiPath,path];
    
    // for migtrate session
    if ([path isEqualToString:getSsecFromTicket] && [self isValidDictionary:sso_header_params])
    {
        NSMutableDictionary *paramsCopy = [sso_header_params mutableCopy];
        [paramsCopy setValue:MIGRATE_TICKETID forKey:SSO_TICKETID_KEY];
        sso_header_params = paramsCopy;
    }
    NSURLComponents * components = [[NSURLComponents alloc] init];
    components.scheme = @"https";
    components.host = baseUrl;
    components.path = fullPath;
    //queries
    if ([self isValidDictionary:queries])
    {
        NSMutableString *q = [[NSMutableString alloc] init];
        for (NSString *key in queries)
        {
            [q appendString:[NSString stringWithFormat:@"%@=%@&",key,queries[key]]];
        }
        if ([q length] > 0)
        {
            q = [[q substringToIndex:[q length] - 1] mutableCopy];
        }
        components.query = q;
    }
    //url
    NSURL * url = components.URL;
    
    //make request
    NSMutableURLRequest * downloadRequest = [NSMutableURLRequest requestWithURL:url];
    switch (scope)
    {
        case syncPixel:
            [downloadRequest setHTTPMethod:@"GET"];
            break;
        default:
            [downloadRequest setHTTPMethod:@"POST"];
            break;
    }
    
    //set HTTP json Body from parameters
    if ([self isValidDictionary:params])
    {
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params
                                                            options:0
                                                              error:&err];
        [downloadRequest setHTTPBody: jsonData];
    }
    else
    {
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:@{}
                                                            options:0
                                                              error:&err];
        [downloadRequest setHTTPBody: jsonData];
    }
    
    for (NSString *key in sso_header_params)
    {
        [downloadRequest setValue:sso_header_params[key] forHTTPHeaderField:key];
    }
    
    //add support for Json
    [downloadRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [downloadRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    return downloadRequest;
}

+(NSMutableDictionary *)getHeadersForSession:(SSOSession *)session
{
    NSMutableDictionary *headerInfo = [[NSMutableDictionary alloc] init];
    
    [headerInfo setValue:session.ssec forKey: SSO_SSEC_KEY];
    [headerInfo setValue:session.ticketId forKey: SSO_TICKETID_KEY];
    [headerInfo setValue:session.tgId forKey: SSO_TGID_KEY];
    [headerInfo setValue:appHeader.channel forKey: SSO_CHANNEL_KEY];
    [headerInfo setValue:appHeader.appVersion forKey: SSO_APP_VERSION_KEY];
    [headerInfo setValue:appHeader.platform forKey: SSO_PLATFORM_KEY];
    
    if (TRACKING_CHANNEL) {
        [headerInfo setValue:TRACKING_CHANNEL forKey: SSO_CHANNEL_KEY];
        TRACKING_CHANNEL = nil;
    }
    return headerInfo;
}

+(NSMutableDictionary *)getSyncHeaders
{
    NSMutableDictionary *headerInfo = [[NSMutableDictionary alloc] init];
    
    [headerInfo setValue:[UIDevice currentDevice].identifierForVendor.UUIDString forKey: @"_col_uuid"];
    
    return headerInfo;
}
+(BOOL)isValidDictionary:(NSDictionary *)data
{
    if ([data isKindOfClass:[NSDictionary class]] && [data count] > 0)
    {
        return true;
    }
    return false;
}

@end
