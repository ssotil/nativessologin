//
//  SSOHeaders.h
//  Pods
//
//  Created by Pankaj Verma on 02/05/17.
//
//

#import <Foundation/Foundation.h>
#define SSOHEADER_CHANNEL_KAY @"channel"
#define SSOHEADER_TEAM_ID_KAY  @"teamId"
#define SSOHEADER_SITE_ID_KAY  @"siteId"
#define SSOHEADER_APP_VERSION_KAY  @"appVersion"
#define SSOHEADER_PALTFORM_KAY  @"platform"
#define SSOHEADER_GET_DATA_KAY  @"getData"


@interface SSOHeaders : NSObject
@property (nonatomic, nullable, readonly) NSString * channel;
@property (nonatomic, nullable, readonly) NSString * teamId;
@property (nonatomic, nullable, readonly) NSString * siteId;
@property (nonatomic, nullable, readonly) NSString * appVersion;
@property (nonatomic, nullable, readonly) NSString * platform;

@property (nonatomic, nullable, readonly) NSString * getData;

- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary;

-(void)setGetData:(NSString * _Nullable)getData;
-(void)setChannel:(NSString * _Nullable)channel;
-(void)setSiteId:(NSString * _Nullable)siteId;
-(void)setTeamId:(NSString * _Nullable)teamId;
@end
