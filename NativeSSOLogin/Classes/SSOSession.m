//
//  SSOSession.m
//  Pods
//
//  Created by Pankaj Verma on 04/04/17.
//
//

#import "SSOSession.h"
#import "SSOKeychain.h"

#define KEYCHAIN_TGID_KEY  @"tgId"
#define KEYCHAIN_SSEC_KEY  @"ssec"
#define KEYCHAIN_TICKET_ID_KEY  @"ticketId"
#define KEYCHAIN_OAUTH_SITEID_KEY  @"oauthsiteid"
#define KEYCHAIN_PRIMARY_EMAIL_KEY  @"PEML"

@interface SSOSession ()
@property (nonatomic, nullable) SSOKeychain *keychain;
@property (nonatomic, nullable) NSString *unverifiedUser;
@end

@implementation SSOSession

@synthesize ssec = _ssec;
@synthesize tgId = _tgId;
@synthesize ticketId = _ticketId;
@synthesize identifier = _identifier;
@synthesize type = _type;


- (nullable instancetype)initWithAppKeychain
{
    if (self = [super init])
    {
        if (self != nil)
        {
            
            _keychain = [[SSOKeychain alloc] initWithService:[[NSBundle mainBundle] bundleIdentifier] withGroup:nil];
            _tgId = [_keychain find:KEYCHAIN_TGID_KEY];
            _ssec = [_keychain find:KEYCHAIN_SSEC_KEY];
            _ticketId = [_keychain find:KEYCHAIN_TICKET_ID_KEY];
            _identifier = [_keychain find:KEYCHAIN_PRIMARY_EMAIL_KEY];
            _type = [_keychain find:KEYCHAIN_OAUTH_SITEID_KEY];

        }
    }
    return self;
}

- (nullable instancetype)initWithSharedKeychain
{
    if (self = [super init])
    {
        if (self != nil)
        {
            

            _tgId = [[SSOKeychain sharedKeychain] find:KEYCHAIN_TGID_KEY];
            _ssec = [[SSOKeychain sharedKeychain] find:KEYCHAIN_SSEC_KEY];
            _ticketId = [[SSOKeychain sharedKeychain] find:KEYCHAIN_TICKET_ID_KEY];
            _identifier = [[SSOKeychain sharedKeychain] find:KEYCHAIN_PRIMARY_EMAIL_KEY];
            _type = [[SSOKeychain sharedKeychain] find:KEYCHAIN_OAUTH_SITEID_KEY];
            
        }
    }
    return self;
}




- (nullable instancetype)initWithDictionary:(nullable NSDictionary *) dictionary
{
    if (self = [super init])
    {
        if (self != nil) {
            if (![dictionary isKindOfClass:[NSNull class]])
            {
                [self setValuesForKeysWithDictionary:dictionary];
            }
        }
    }
    return self;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
    NSLog(@"undefined key  = %@",key);
    
}

//remove
-(void)removeAppSessionSsec
{
    _ssec = nil;
    [_keychain remove:KEYCHAIN_SSEC_KEY];
}
-(void)removeAppSessionTicketId
{
    _ticketId = nil;
    [_keychain remove:KEYCHAIN_TICKET_ID_KEY];
}
-(void)removeAppSessionIdentifier
{
    _identifier = nil;
    [_keychain remove:KEYCHAIN_PRIMARY_EMAIL_KEY];
}
-(void)removeAppSessionType
{
    _type = nil;
    [_keychain remove:KEYCHAIN_OAUTH_SITEID_KEY];
}

-(void)removeAppSessionTgId
{
    _tgId = nil;
    [_keychain remove:KEYCHAIN_TGID_KEY];
}

//remove
-(void)removeSharedSessionSsec
{
    _ssec = nil;
    [[SSOKeychain sharedKeychain] remove:KEYCHAIN_SSEC_KEY];
}
-(void)removeSharedSessionTicketId
{
    _ticketId = nil;
    [[SSOKeychain sharedKeychain] remove:KEYCHAIN_TICKET_ID_KEY];
}
-(void)removeSharedSessionIdentifier
{
    _identifier = nil;
    [[SSOKeychain sharedKeychain] remove:KEYCHAIN_PRIMARY_EMAIL_KEY];
}
-(void)removeSharedSessionType
{
    _type = nil;
    [[SSOKeychain sharedKeychain] remove:KEYCHAIN_OAUTH_SITEID_KEY];
}

-(void)removeSharedSessionTgId
{
    _tgId = nil;
    [[SSOKeychain sharedKeychain] remove:KEYCHAIN_TGID_KEY];
}

//set
-(void)setTgId:(NSString * _Nullable)tgId
{
     _tgId = tgId;
}
-(void)setSsec:(NSString * _Nullable)ssec
{
    _ssec = ssec;
}
-(void)setTicketId:(NSString * _Nullable)ticketId
{
    _ticketId = ticketId;
}

-(void)setType:(NSString * _Nullable)type
{
    _type = type;

}

-(void)setIdentifier:(NSString * _Nullable)identifier
{
    _identifier = identifier;
}


-(NSString *_Nullable)ssec
{
    return _ssec;
}
-(NSString *_Nullable)ticketId
{
    return _ticketId;
}

-(NSString *_Nullable)tgId
{
    return _tgId;
}

-(NSString *_Nullable)type
{
    return _type;
}

-(NSString *_Nullable)identifier
{
    return _identifier;
}

-(NSString *_Nullable)unverifiedUser
{
    return _unverifiedUser;
}


-(void)saveUserDetailsToKeychain:(SSOUserDetails *)userDetails
{
    NSData * arcData =  [NSKeyedArchiver archivedDataWithRootObject:userDetails];
    [_keychain insert:@"userDetails" :arcData];
}
-(void)RemoveUserDetailsFromKeychain
{
    [_keychain remove:@"userDetails"];
}
-(SSOUserDetails *) getUserDetailsFromKeychain
{
    NSData *encodedObject = [_keychain findDataForKey:@"userDetails"];
    SSOUserDetails *userDetails = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return userDetails;
}
-(void)initializeAppSessionWithSession:(SSOSession *)session
{
  //ssec
    if ([session.ssec isKindOfClass:NSString.class])
    {
        _ssec = session.ssec;
        [_keychain insert:KEYCHAIN_SSEC_KEY :[session.ssec dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (session.ssec == nil)
            {
                 [_keychain insert:KEYCHAIN_SSEC_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
            }
    //ticketId
        if ([session.ticketId isKindOfClass:NSString.class])
        {
        _ticketId = session.ticketId;
        [_keychain insert:KEYCHAIN_TICKET_ID_KEY :[session.ticketId dataUsingEncoding:NSUTF8StringEncoding]];
        }
        if (session.ticketId == nil)
        {
            [_keychain insert:KEYCHAIN_TICKET_ID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
        }
    //identifier
        if ([session.identifier isKindOfClass:NSString.class])
        {
        _identifier = session.identifier;
        [_keychain insert:KEYCHAIN_PRIMARY_EMAIL_KEY :[session.identifier dataUsingEncoding:NSUTF8StringEncoding]];
        }
         if(session.identifier == nil)
             [_keychain insert:KEYCHAIN_PRIMARY_EMAIL_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    //type
        if ([session.type isKindOfClass:NSString.class])
        {
            _type = session.type;
            [_keychain insert:KEYCHAIN_OAUTH_SITEID_KEY :[session.type dataUsingEncoding:NSUTF8StringEncoding]];
        }
        if (session.type == nil)
        {
             [_keychain insert:KEYCHAIN_OAUTH_SITEID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
        }
}
-(void)initializeSharedSessionWithSession:(SSOSession *)session
{
    //ssec
    if ([session.ssec isKindOfClass:NSString.class])
    {
        _ssec = session.ssec;
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_SSEC_KEY :[session.ssec dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (session.ssec == nil)
    {
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_SSEC_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    //ticketId
    if ([session.ticketId isKindOfClass:NSString.class])
    {
        _ticketId = session.ticketId;
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_TICKET_ID_KEY :[session.ticketId dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (session.ticketId == nil)
    {
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_TICKET_ID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    //identifier
    if ([session.identifier isKindOfClass:NSString.class])
    {
        _identifier = session.identifier;
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_PRIMARY_EMAIL_KEY :[session.identifier dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if(session.identifier == nil)
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_PRIMARY_EMAIL_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    //type
    if ([session.type isKindOfClass:NSString.class])
    {
        _type = session.type;
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_OAUTH_SITEID_KEY :[session.type dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (session.type == nil)
    {
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_OAUTH_SITEID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    }

}
-(void)initializeAppSessionWithTgId:(NSString *)tgId
{
        if ([tgId isKindOfClass:NSString.class])
        {
            _tgId = tgId;
            [_keychain insert:KEYCHAIN_TGID_KEY :[tgId dataUsingEncoding:NSUTF8StringEncoding]];
        }
        if (tgId == nil)
        {
            [_keychain insert:KEYCHAIN_TGID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
        }
}
-(void)initializeGlobalSessionWithTgId:(NSString *)tgId
{
    if ([tgId isKindOfClass:NSString.class])
    {
        _tgId = tgId;
        [[SSOKeychain sharedKeychain] insert:KEYCHAIN_TGID_KEY :[tgId dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (tgId == nil)
    {
        [[SSOKeychain sharedKeychain]  insert:KEYCHAIN_TGID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    }
}
-(void)setAppSessionNewTicketId:(NSString *)ticketId
{
    if ([ticketId isKindOfClass:NSString.class])
    {
        _ticketId = ticketId;
        [_keychain insert:KEYCHAIN_TICKET_ID_KEY :[ticketId dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (ticketId == nil)
    {
        [_keychain  insert:KEYCHAIN_TICKET_ID_KEY :[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    }

}
-(void)updateAppSession
{
            _tgId = [_keychain find:KEYCHAIN_TGID_KEY];
            _ssec = [_keychain find:KEYCHAIN_SSEC_KEY];
            _ticketId = [_keychain find:KEYCHAIN_TICKET_ID_KEY];
            _identifier = [_keychain find:KEYCHAIN_PRIMARY_EMAIL_KEY];
            _type = [_keychain find:KEYCHAIN_OAUTH_SITEID_KEY];
}

-(void)updateGlobalSession
{    
    _tgId = [[SSOKeychain sharedKeychain] find:KEYCHAIN_TGID_KEY];
    _ssec = [[SSOKeychain sharedKeychain] find:KEYCHAIN_SSEC_KEY];
    _ticketId = [[SSOKeychain sharedKeychain] find:KEYCHAIN_TICKET_ID_KEY];
    _identifier = [[SSOKeychain sharedKeychain] find:KEYCHAIN_PRIMARY_EMAIL_KEY];
    _type = [[SSOKeychain sharedKeychain] find:KEYCHAIN_OAUTH_SITEID_KEY];
    

}
@end
