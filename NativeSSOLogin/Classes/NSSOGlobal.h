//
//  NSSOGlobal.h
//  Pods
//
//  Created by Pankaj Verma on 10/13/16.
//
//

#import <Foundation/Foundation.h>
#import "SSOSession.h"
#import "SSOHeaders.h"
#define CASE(str)                       if ([__s__ isEqualToString:(str)])
#define SWITCH(s)                       for (NSString *__s__ = (s); ; )
#define DEFAULT


typedef enum ApiScope : NSUInteger {
    ssomSocial=2,
    ssoGlobal,
    ssoImageUpload,
    ssoBase,
    syncPixel
} ApiScope;

@interface NSSOGlobal : NSObject

////Relative Urls for SSOBaseUrl

FOUNDATION_EXPORT NSString * const getDataForDeviceUrlPath; //2
FOUNDATION_EXPORT NSString * const getSsecFromTicket;//2
FOUNDATION_EXPORT NSString * MIGRATE_TICKETID;
FOUNDATION_EXPORT NSString * UUID_BOUNDARY;
FOUNDATION_EXPORT NSString * TRACKING_CHANNEL;

FOUNDATION_EXPORT SSOSession * appSession;
FOUNDATION_EXPORT SSOSession * globalSession;
FOUNDATION_EXPORT SSOHeaders * appHeader;
@end

